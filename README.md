["Is ", "every ", "person ", "here ", "a ", "mental ", "case ", "?"]
--------------------------------------------------------------------

 - template engine: hakyll
 - html engine: Lucid
 - css engine: clay + styl
 - js: hjsmin (want more?)

[![Twitter][]](http://www.twitter.com/Cynede)

Building
========

 - install hakyll, hjsmin, lucid, clay ...
 - run `update.cmd` on Windows
 - run `update.sh` on Posix

TODO
====

 - refactor project (separate hs / html / css / js folders)
 - explore sane js alternative...
 - replace update.cmd / update.sh with something consistent
 - build whole thing with stack?

![](http://fc09.deviantart.net/fs71/f/2013/195/4/0/mio_by_kigyn-d6dfeuf.png)
[Twitter]: http://mxtoolbox.com/Public/images/twitter-icon.png
